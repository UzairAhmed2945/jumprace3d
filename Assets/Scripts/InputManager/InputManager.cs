using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    #region Data
    public static InputManager Instance;
    [SerializeField] private Vector2 DragSensitivity;

    public Vector2 DeltaDrag { get { return m_DeltaDrag * DragSensitivity; } }
    private Vector2 m_DeltaDrag;

    public Vector2 Drag { get { return m_Drag * DragSensitivity ; } }
    private Vector2 m_Drag;

    private Vector3 m_DownInputPos;
    private Vector3 m_InputLastPos;


    public bool IsInputDown { get { return m_IsInputDown; } }
    private bool m_IsInputDown;
    #endregion Data

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_IsInputDown)
        {
            m_Drag = Input.mousePosition - m_DownInputPos;
            m_DeltaDrag = Input.mousePosition - m_InputLastPos;
            m_InputLastPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonDown(0))
        {
            
            InputDown();
        }

        if (Input.GetMouseButtonUp(0))
        {
            InputUp();
        }
    }
    

    public void InputDown()
    {
        m_IsInputDown = true;
        m_DownInputPos = m_InputLastPos = Input.mousePosition;
        ResetInputValue();
    }

    public void InputUp()
    {
        m_IsInputDown = false;
        ResetInputValue();
    }

    private void ResetInputValue()
    {
        m_DeltaDrag = m_Drag = Vector2.zero;
    }
}
