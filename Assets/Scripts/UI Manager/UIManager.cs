using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Data
    public static UIManager Instance;
    [Space]
    [Header("GamePanels")]
    [SerializeField] private GameObject m_GameCompletePanel;
    [SerializeField] private GameObject m_GameFailPanel;
    [SerializeField] private GameObject m_GameStartPanel;

    [Space]
    [Header("GamePanelButtons")]
    [SerializeField] private Button m_StartButton;
    [SerializeField] private Button m_ResetButton;
    [SerializeField] private Button m_NextButton;

    [Space]
    [Header("Level No")]

    [SerializeField] private TextMeshProUGUI m_LevelText;

    [Space]
    [Header("Coins")]

    [SerializeField] private TextMeshProUGUI m_CompleteCoinsCount;
    [SerializeField] private TextMeshProUGUI m_TopPanelCoinsCount;

    [Space]
    [SerializeField] private Color m_StarsOffColor;
    [SerializeField] private Image [] m_StarsImage;

    #endregion Data

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        m_StartButton.onClick.AddListener(OnClickStartButton);
        m_ResetButton.onClick.AddListener(ClickReset);
        m_NextButton.onClick.AddListener(ClickNext);
        GameManagerDelegate.OnGameReset += OnClickResetButton;
        GameManagerDelegate.OnGameFail += ShowGameFailPanel;
        GameManagerDelegate.OnGameComplete += ShowGameCompletePanel;
        SetCoins();
    }

    private void OnDisable()
    {
        m_StartButton.onClick.RemoveListener(OnClickStartButton);
        m_ResetButton.onClick.RemoveListener(ClickReset);
        m_NextButton.onClick.RemoveListener(ClickNext);
        GameManagerDelegate.OnGameReset -= OnClickResetButton;
        GameManagerDelegate.OnGameFail -= ShowGameFailPanel;
        GameManagerDelegate.OnGameComplete -= ShowGameCompletePanel;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_LevelText.text = "Level "+(StorageManager.Instance.CurrentlevelNo + 1).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    #region GameStates
    void OnClickStartButton()
    {

        StartCoroutine(ClickStartButton());
    }

    IEnumerator ClickStartButton()
    {
        m_GameStartPanel.SetActive(false);
        yield return new WaitForSeconds(0f);
        GameManager.Instance.Gamestart();

    }

    void OnClickResetButton()
    {
        ResetStars();
        StartCoroutine(ClickResetButton());
    }

    IEnumerator ClickResetButton()
    {
        yield return new WaitForSeconds(0.1f);
        if (m_GameFailPanel.activeSelf)
        {
            m_GameFailPanel.SetActive(false);
        }
        if (m_GameCompletePanel.activeSelf)
        {
            m_GameCompletePanel.SetActive(false);
        }
        //ClickReset();
        yield return new WaitForSeconds(0.1f);
        m_GameStartPanel.SetActive(true);

    }

    void ShowGameCompletePanel()
    {
        StartCoroutine(GameCompletePanel());
    }

    IEnumerator GameCompletePanel()
    {
        IncreseCoinsOnComplete();
        yield return new WaitForSeconds(1.5f);
        m_GameCompletePanel.SetActive(true);
        StartCoroutine(ChangeStarColors());
    }



    void ShowGameFailPanel()
    {
        StartCoroutine(GameFailPanel());
    }

    IEnumerator GameFailPanel()
    {
        yield return new WaitForSeconds(1f);
        m_GameFailPanel.SetActive(true);
    }

    void ClickReset()
    {
        
        GameManager.Instance.GameReset();
    }

    void ClickNext()
    {
        m_LevelText.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
        GameManager.Instance.GameReset();
    }


    #endregion GameStates


    #region Coins

    private void IncreseCoinsOnComplete()
    {
        StorageManager.Instance.Coins += 100;
        m_CompleteCoinsCount.text = (StorageManager.Instance.Coins).ToString();
        m_TopPanelCoinsCount.text = (StorageManager.Instance.Coins).ToString();
    }

    private void SetCoins()
    {
        m_CompleteCoinsCount.text = (StorageManager.Instance.Coins).ToString();
        m_TopPanelCoinsCount.text = (StorageManager.Instance.Coins).ToString();
    }

    #endregion Coins

    #region Stars

    private IEnumerator ChangeStarColors()
    {
        yield return new WaitForSeconds(0.2f);
        m_StarsImage[0].color = Color.white;
        yield return new WaitForSeconds(0.3f);
        m_StarsImage[1].color = Color.white;
        yield return new WaitForSeconds(0.3f);
        m_StarsImage[2].color = Color.white;
    }

    private void ResetStars()
    {
        for (int i = 0; i < m_StarsImage.Length; i++)
        {
            m_StarsImage[i].color = m_StarsOffColor;
        }
    }

    #endregion Stars
}
