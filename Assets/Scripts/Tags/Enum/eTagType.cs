
public enum eTagType 
{
   Platform,
   Water,
   Player,
   Complete,
   PlatformLong,
   Enemy
}
