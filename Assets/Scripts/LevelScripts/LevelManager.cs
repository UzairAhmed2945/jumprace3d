using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    #region Data
    public LevelScript[] LevelScripts;
    public LevelScript CurrentLevel;
    #endregion Data

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += setLevel;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= setLevel;
    }
    // Start is called before the first frame update
    void Start()
    {
        setLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void setLevel()
    {
        closeAllLevels();
        var levelNo = StorageManager.Instance.CurrentlevelNo % LevelScripts.Length;
        LevelScripts[levelNo].gameObject.SetActive(true);
        LevelScripts[levelNo].SetPlayer(LevelScripts[levelNo].PLayerPosition, LevelScripts[levelNo].PlayerRotation);
        CurrentLevel = LevelScripts[levelNo];


    }

    void closeAllLevels()
    {
        for (int i = 0; i < LevelScripts.Length; i++)
        {
            LevelScripts[i].gameObject.SetActive(false);
        }
    }
}
