using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScript : MonoBehaviour
{
    #region Data
    public Vector3 PLayerPosition;
    public Vector3 PlayerRotation;
    #endregion Data
    private void OnEnable()
    {
        
    }
    public void SetPlayer(Vector3 Playerposition,Vector3 Playerrotation)
    {
        GameManager.Instance.PlayerScript.SetPlayerValues(Playerposition, Playerrotation);
    }
}
