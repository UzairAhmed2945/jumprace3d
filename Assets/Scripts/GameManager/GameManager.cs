using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    #region Data
    public static GameManager Instance;
    [Space]
    public eGameStates e_gamestates = eGameStates.Idle;
    public Player PlayerScript;
    #endregion Data
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Gamestart()
    {
        GameManager.Instance.e_gamestates = eGameStates.Playing;
        GameManagerDelegate.GameStart();
    }

    public void GameFail()
    {
        GameManager.Instance.e_gamestates = eGameStates.Fail;
        GameManagerDelegate.GameFail();
        
    }

    public void GameReset()
    {
        GameManager.Instance.e_gamestates = eGameStates.Idle;
        GameManagerDelegate.GameReset();
    }

    public void GameComplete()
    {
        GameManager.Instance.e_gamestates = eGameStates.Complete;
        StorageManager.Instance.CurrentlevelNo++;
        StorageManager.Instance.ActuallevelNo++;
        GameManagerDelegate.GameComplete();
       
    }
}
