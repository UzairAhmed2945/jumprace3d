using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTriggerScript : MonoBehaviour
{
    #region Data
    private int m_JumpCount;
    private int m_EnemyJumpCount;
    [SerializeField] private PlatformScript m_ParentPlatform;
    [SerializeField] private ParticleSystem m_CollisionParticle;
    #endregion Data


    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += ResetTrgger;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetTrgger;
    }

    private void OnCollisionEnter(Collision collision)
    {
        m_CollisionParticle.Play();
        if (collision.transform.CompareTag(nameof(eTagType.Player)))
        {

            if (m_JumpCount == 1) {
                m_JumpCount = 0;
                RotateTowardsNextPlatform(collision.gameObject);
                if (m_ParentPlatform.isBreakable)
                {
                    m_ParentPlatform.PlatformExplode(collision.transform);

                }
            }
            m_JumpCount++;
        }

        if (collision.transform.CompareTag(nameof(eTagType.Enemy)))
        {
            RotateTowardsNextPlatform(collision.gameObject);
            if (m_EnemyJumpCount == 1)
            {
                m_EnemyJumpCount = 0;
                if (m_ParentPlatform.isBreakable)
                {
                    m_ParentPlatform.PlatformExplode(collision.transform);

                }
            }
            m_EnemyJumpCount++;
        }
    }


    public void RotateTowardsNextPlatform(GameObject characterTriggered)
    {
        if (m_ParentPlatform.NextPlatfrom != null)
        {
            var lookPos = m_ParentPlatform.NextPlatfrom.position - characterTriggered.transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            characterTriggered.transform.DORotateQuaternion(rotation, 0.25f);
        }
        
    }

    public Transform GetNextTarget()
    {
        return m_ParentPlatform.NextPlatfrom;
    }

    void ResetTrgger()
    {
        m_JumpCount = 0;
        m_EnemyJumpCount = 0;
    }

    


}
