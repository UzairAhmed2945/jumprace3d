using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlatformScript : MonoBehaviour
{

    #region Data
    [SerializeField] public Transform NextPlatfrom;
    [SerializeField] private int m_PlatformNumber;
    [SerializeField] private Fragments[] m_fragments;
    [SerializeField] private Collider m_PlatformCollider;
    [SerializeField] private Animator m_PlatfromAnimator;
    [SerializeField] private TextMeshPro m_PlatformText;
    public bool isBreakable;
    [SerializeField] private float m_TimeToHeal = 5f;

    #endregion Data
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        m_PlatformText.text = m_PlatformNumber.ToString();
        GameManagerDelegate.OnGameReset += ResetPlatform;

    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetPlatform;
    }

    public void PlatformExplode(Transform ForceObject)
    {
        m_PlatfromAnimator.enabled = false;
        for (int i= 0; i< m_fragments.Length; i++)
        {
            m_fragments[i].Explode(ForceObject);
        }
        m_PlatformCollider.enabled = false;
        m_PlatformText.gameObject.SetActive(false);
        StartCoroutine(I_HealPlatform());
        
    }

    public void ResetPlatform()
    {
        if (isBreakable)
        {
            for (int i = 0; i < m_fragments.Length; i++)
            {
                m_fragments[i].ResetFragments();
            }
        }
        m_PlatfromAnimator.Rebind();
        m_PlatfromAnimator.enabled = true;
        m_PlatformCollider.enabled = true;
        m_PlatformText.gameObject.SetActive(true);

    }

    IEnumerator I_HealPlatform()
    {
        yield return new WaitForSeconds(m_TimeToHeal);
        for (int i = 0; i < m_fragments.Length; i++)
        {
            m_fragments[i].HealPlatform();
        }
        m_PlatformCollider.enabled = true;
        m_PlatformText.gameObject.SetActive(true);
        m_PlatfromAnimator.enabled = false;
    }


    
}
