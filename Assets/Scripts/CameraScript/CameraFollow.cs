using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    #region Data
    [SerializeField] private GameObject m_Target;
    [SerializeField] private GameObject m_Player;

    [SerializeField] private float m_SmoothSpeed = 0.125f;
    [SerializeField] private Vector3 m_Offset;
    #endregion Data

    void FixedUpdate()
    {

        Vector3 OffsetPosition = m_Target.transform.position + m_Offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, OffsetPosition, m_SmoothSpeed);

        transform.position = smoothedPosition;
        transform.LookAt(m_Player.transform.position); 
    }
}
