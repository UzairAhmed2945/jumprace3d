using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    #region Data
    [SerializeField] private CharacterController m_CharacterController;

    private int m_JumpCount;
    private bool m_CanMoveForward;
    private GameObject m_CurrentPlatform;
    private Vector3 m_EnemyInitialPosition;
    private Quaternion m_EnemyInitialRotation;
    [SerializeField] private Animator m_EnemyAnimator;

    #endregion Data
    private void Awake()
    {
        m_EnemyInitialPosition = this.transform.position;
        m_EnemyInitialRotation = this.transform.rotation;
    }

    private void OnEnable()
    {
        ResetEnemy();

        GameManagerDelegate.OnGameReset += ResetEnemy;  
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetEnemy;
    }

    public void Update()
    {
        
    }

    public void HitPlatform(GameObject triggeredPlatform)
    {
        if (m_CurrentPlatform == null)
            m_CurrentPlatform = triggeredPlatform;

        if (m_CurrentPlatform == triggeredPlatform)
        {
            int RandomJumpCount = Random.Range(0,1);
            m_JumpCount++;
            if (m_JumpCount > RandomJumpCount)
            {
                m_JumpCount = 0;
                m_CanMoveForward = true;
                m_CharacterController.JumpEnemy(triggeredPlatform.transform.position +  new Vector3(0,0.1f,0));
            }
        }
        else
        {
            m_JumpCount = 0;
            m_CanMoveForward = false;
            m_CurrentPlatform = triggeredPlatform;
        }
    }

    public void KillJump()
    {
        m_CharacterController.KillJump();
    }

    void ResetEnemy()
    {
        KillJump();
        this.transform.position = m_EnemyInitialPosition;
        this.transform.rotation = m_EnemyInitialRotation;
        m_EnemyAnimator.SetBool(nameof(eAnimType.Dance), false);
    }
}
