using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Data
    [SerializeField] private Animator m_PlayerAnimator;
    [SerializeField] private CharacterController m_CharaterCintroller;
    #endregion Data
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (InputManager.Instance.IsInputDown && GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            m_CharaterCintroller.CharacterForwardMovement();
            m_CharaterCintroller.CharacterSideMovement();
        }

    }



    public void SetPlayerValues(Vector3 Playerposition,Vector3 Playerrotation)
    {
        this.transform.position = Playerposition;
        this.transform.eulerAngles = Playerrotation;
        m_PlayerAnimator.SetBool(nameof(eAnimType.Dance), false);

    }
}
