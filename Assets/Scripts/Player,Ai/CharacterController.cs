using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    #region Data
    [SerializeField] private float m_CharacterForwardSpeed;
    [SerializeField] private float m_CharacterRotationSpeed;
    [SerializeField] private Rigidbody m_CharacterRigidbody;
    [SerializeField] private float m_CharacterMaxSpeed;
    [SerializeField] private float m_CharacterJumptSpeed;
    public float CharacterJumpSpeed{get { return m_CharacterJumptSpeed; }set { m_CharacterJumptSpeed = value;}}
    public float NormalJumpSpeed;
    public float BoostJumpSpeed;
    [SerializeField] private Vector3 m_Jumpdirection = Vector3.up;

    #endregion Data

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       
    }

    #region Character Movement

    public void CharacterForwardMovement()
    {
        transform.position += transform.forward * Time.deltaTime * m_CharacterForwardSpeed;
    }

    public void CharacterSideMovement()
    {
        float XaxisRotation = -InputManager.Instance.DeltaDrag.x * m_CharacterRotationSpeed * Time.deltaTime;

        transform.Rotate(Vector3.down, XaxisRotation);
    }

    public void CharacterJump()
    {
        m_CharacterRigidbody.velocity = Vector3.zero;

        if (Vector3.Project(m_CharacterRigidbody.velocity, m_Jumpdirection).magnitude < m_CharacterMaxSpeed)
        {
            m_CharacterRigidbody.AddForce(m_Jumpdirection * m_CharacterJumptSpeed);
        }
    }
    

    public void JumpEnemy(Vector3 currentTarget)
    {
        m_CharacterRigidbody.DOJump(currentTarget, 6, 0, 3).OnComplete(()=>{  });
    }

    public void KillJump()
    {
        m_CharacterRigidbody.DOKill();
    }

    #endregion Character Movement
}
