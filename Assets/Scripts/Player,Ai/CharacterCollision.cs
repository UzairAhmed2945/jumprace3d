using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCollision : MonoBehaviour
{

    #region Data

    [SerializeField] private Animator m_CharacterAnimator;
    [SerializeField] private CharacterController m_CharacterController;
    [SerializeField] private ParticleSystem m_WaterPartile;

    [SerializeField] private bool m_IsEnemy;
    [SerializeField] private Enemy m_Enemy;

    #endregion Data



    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(nameof(eTagType.Platform)))
        {
            m_CharacterController.CharacterJumpSpeed = m_CharacterController.NormalJumpSpeed;
            m_CharacterController.CharacterJump();
            m_CharacterAnimator.SetTrigger(nameof(eAnimType.Jump));
            m_CharacterAnimator.SetFloat("Blend",Random.Range(0,2));

            if (m_IsEnemy)
            {
                if (GameManager.Instance.e_gamestates == eGameStates.Playing)
                {
                    m_Enemy.KillJump();
                    m_Enemy.HitPlatform(collision.gameObject.GetComponent<PlatformTriggerScript>().GetNextTarget().gameObject);
                }
            }
        }

        if (collision.transform.CompareTag(nameof(eTagType.PlatformLong)))
        {
            m_CharacterController.CharacterJumpSpeed = m_CharacterController.BoostJumpSpeed;
            m_CharacterController.CharacterJump();
            m_CharacterAnimator.SetTrigger(nameof(eAnimType.Jump));
            m_CharacterAnimator.SetFloat("Blend", Random.Range(0, 2));

            if (m_IsEnemy)
            {
                if (GameManager.Instance.e_gamestates == eGameStates.Playing)
                {
                    m_Enemy.KillJump();
                    m_Enemy.HitPlatform(collision.gameObject.GetComponent<PlatformTriggerScript>().GetNextTarget().gameObject);
                }
            }

        }

        if (collision.transform.CompareTag(nameof(eTagType.Complete)))
        {
            m_CharacterAnimator.SetBool(nameof(eAnimType.Dance),true);
            if (!m_IsEnemy)
            {
                GameManager.Instance.GameComplete();
            }

        }

        if (collision.transform.CompareTag(nameof(eTagType.Water)))
        {
            m_WaterPartile.Play();
            if (!m_IsEnemy)
            {
                GameManager.Instance.GameFail();
            }

        }
    }
}
