using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteScript : MonoBehaviour
{
    #region Data
    [SerializeField] private ParticleSystem[] m_CompleteParticles;

    #endregion Data
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(nameof(eTagType.Player)))
        {
            for(int i = 0; i < m_CompleteParticles.Length; i++)
            {
                m_CompleteParticles[i].Play();
            }
           
        }
    }
}
