using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformFinder : MonoBehaviour
{
    #region Data
    [SerializeField] private LineRenderer m_LineRenderer;
    [SerializeField] private float m_MaxLength;

    [SerializeField] private Color m_OnPlatform, m_OutPlatform;
    [SerializeField] private Vector3 m_Offset;
    public LayerMask m_IgnoreMask;
    [SerializeField] private Renderer m_SightMaterial;
    [SerializeField] private Transform m_SightObject;

    #endregion Data

    private void Start()
    {
    }

    private void Update()
    {
        PlatformHit(Vector3.down,m_MaxLength,m_SightObject.position + m_Offset);
    }

    Vector3 PlatformHitCheck(Vector3 startingPosition, float distance, Vector3 direction)
    {
        Ray ray = new Ray(startingPosition, direction);
        RaycastHit hit;
        Vector3 endPos = startingPosition + (distance * direction);

        if (Physics.Raycast(ray, out hit, distance, ~m_IgnoreMask) && !hit.collider.transform.CompareTag(nameof(eTagType.Water)))
        {
            endPos = hit.point;
            m_LineRenderer.material.color = m_OnPlatform;
            return endPos;
        }
        else
        {
            m_LineRenderer.material.color = m_OutPlatform;
        }

        return endPos;
    }

    public void PlatformHit(Vector3 direction,float scale,Vector3 StartingPosition)
    {
        Ray ray = new Ray(StartingPosition, direction);
        RaycastHit hit;
        Vector3 endPos;
        m_SightObject.localScale = new Vector3(m_SightObject.localScale.x, scale, m_SightObject.localScale.z);

        
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~m_IgnoreMask) && !hit.collider.transform.CompareTag(nameof(eTagType.Water)))
        {
            endPos = hit.point;
            m_SightMaterial.material.SetColor("_Color", m_OnPlatform);

            m_SightObject.localScale = new Vector3(m_SightObject.lossyScale.x, hit.distance/2, m_SightObject.lossyScale.z);
        }
        else
        { 
            m_SightMaterial.material.SetColor("_Color", m_OutPlatform);
        }


    }
}
