using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageManager : MonoBehaviour
{
    #region Data
    public static StorageManager Instance;
    public int CurrentlevelNo { get { return PlayerPrefs.GetInt(nameof(CurrentlevelNo),0); } set { PlayerPrefs.SetInt((nameof(CurrentlevelNo)), value); } }
    public int ActuallevelNo { get { return PlayerPrefs.GetInt(nameof(ActuallevelNo), 0); } set { PlayerPrefs.SetInt((nameof(ActuallevelNo)), value); } }
    public int Coins { get { return PlayerPrefs.GetInt(nameof(Coins)); } set { PlayerPrefs.SetInt((nameof(Coins)), value); } }
    #endregion Data

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
